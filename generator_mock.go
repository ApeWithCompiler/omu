package omu

// GeneratorMock is a mock for testing
// Assignes key as value, not recommended to use outside testing
type GeneratorMock struct {}

func NewGeneratorMock() GeneratorMock {
	return GeneratorMock{}
}

func (g *GeneratorMock) GenerateValue(key string) (string, error) {
	return key, nil
}
