package omu

import (
	"testing"
)

func TestHashMappingTable_SetKey(t *testing.T) {
	sut := NewHashMappingTable()

	key := "baz"
	want := "foobar"

	sut.SetKey(key, want)

	got, _ := sut.GetKey(key)
	if got != want {
		t.Errorf("GetKey() returned %v, want %v", got, want)
	}
}

func TestHashMappingTable_UnsetKey(t *testing.T) {
	sut := NewHashMappingTable()

	key := "baz"
	want := ""

	sut.SetKey(key, "foobar")
	sut.UnsetKey(key)

	got, _ := sut.GetKey(key)
	if got != want {
		t.Errorf("GetKey() returned %v, want empty because of UnsetKey()", got)
	}
}
