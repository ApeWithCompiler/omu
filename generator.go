package omu

// Generator is used to generate values for the objectmappingunit
// uppon setting a new key
type Generator interface {
	GenerateValue(key string) (string, error)
}
