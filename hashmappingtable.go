package omu

// HashMappingTable implements a simple objectmappingtable as a hashmap
// Usefull for testing purposes, persistent implementations are recommended.
type HashMappingTable struct {
	m map[string]string
}

func NewHashMappingTable() HashMappingTable {
	m := make(map[string]string)
	return HashMappingTable{
		m: m,
	}
}

func (h *HashMappingTable) SetKey(key, value string) error {
	h.m[key] = value
	return nil
}

func (h *HashMappingTable) UnsetKey(key string) error {
	delete(h.m, key)
	return nil
}

func (h *HashMappingTable) GetKey(key string) (string, error) {
	val, _ := h.m[key]
	return val, nil
}
