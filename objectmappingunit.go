package omu

import "errors"

type ObjectMappingUnit struct {
	Generator Generator
	Table ObjectMappingTable
	AllowOverwrites bool
}

func NewObjectMappingUnit() ObjectMappingUnit {
	g := NewGeneratorMock()
	t := NewHashMappingTable()

	return ObjectMappingUnit{
		Generator: &g,
		Table: &t,
		AllowOverwrites: false,
	}
}

// NewMapping creates a new key value mapping
// The objectmappingunit is also responsible for creating the value
// according to its requirements
func (omu *ObjectMappingUnit) NewMapping(key string) (string, error) {
	if !omu.AllowOverwrites {
		val, err := omu.Table.GetKey(key)
		if err != nil {
			return "", err
		}

		if val != "" {
			return val, errors.New("Key allready mapped")
		}
	}

	val, err := omu.Generator.GenerateValue(key)
	if err != nil {
		return "", err
	}

	err = omu.Table.SetKey(key, val)
	if err != nil {
		return "", err
	}

	return val, nil
}

// RemoveMapping removes a mapping
func (omu *ObjectMappingUnit) RemoveMapping(key string) error {
	return omu.Table.UnsetKey(key)
}

// GetMapping retrives the value for a key value mapping
func (omu *ObjectMappingUnit) GetMapping(key string) (string, error) {
	val, err := omu.Table.GetKey(key)
	if err != nil {
		return "", err
	}

	if val == "" {
		return "", errors.New("Key not mapped")
	}

	return val, nil
}
