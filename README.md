# Object Mapping Unit

A module meant to encapsulate a mechanism to manage key/value relationships.
It differs from a simple hashmap as it additionaly responsible of generating the values.

The background was in different applications a mechanism was needed to evenly balance a large amount of files into different subdirectories. Sometimes the filepaths were not be able to derive from the key, so a mapping mechanism was additionally needed.
The logic of doing this spilled into the client. With the Object Mapping Unit the responsiblity is moved away from the client.

The table containing the mapping and the function generating the values are defined as interfaces and may be implemented speciffic to the needs.

## Usage

Bellow is the usage of a Object Mapping Unit. It is recommended to use a different Mapping Table implementation if persistence is needed.
Additionally the Value Generator Mock defeats the actual purpose, returning the key as its value.

For Value Generators specialized on balancing files see additionally [omu-fs-generator](https://gitlab.com/ApeWithCompiler/omu-fs-generator).

```go
// Create a in memory version with a generator mock
// Different table and generator implementations are recommended
mapper := omu.NewObjectMappingUnit()

// Creates a new value and mapps it to key
value, err := mapper.NewMapping("mykey")


// Retrive the mapped value anytime later
value, err := mapper.GetMapping("mykey")

// Remove the mapping if not needed
err := mapper.RemoveMapping("mykey")
```
