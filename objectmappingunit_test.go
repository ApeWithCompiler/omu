package omu

import (
	"testing"
)

func TestObjectMappingUnit_NewMapping(t *testing.T) {
	sut := NewObjectMappingUnit()

	key := "baz"
	want := "baz"

	got, _ := sut.NewMapping(key)

	if got != want {
		t.Errorf("NewMapping() returned %v, want %v", got, want)
	}
}

func TestObjectMappingUnit_NewMappingOverwriteError(t *testing.T) {
	sut := NewObjectMappingUnit()

	key := "baz"
	want := "Key allready mapped"

	sut.NewMapping(key)
	_, got := sut.NewMapping(key) //attempt disallowed overwrite, should error

	if got.Error() != want {
		t.Errorf("NewMapping() returned error %v, want %v", got, want)
	}
}

func TestObjectMappingUnit_NewMappingOverwriteAllowed(t *testing.T) {
	sut := NewObjectMappingUnit()
	sut.AllowOverwrites = true

	key := "baz"
	want := "baz"

	sut.NewMapping(key)
	got, err := sut.NewMapping(key)

	if err != nil {
		t.Errorf("NewMapping() returned error, but shouldn't %v", err)
	}

	if got != want {
		t.Errorf("NewMapping() returned %v, want %v", got, want)
	}
}

func TestObjectMappingUnit_GetMapping(t *testing.T) {
	sut := NewObjectMappingUnit()

	key := "baz"
	want := "baz"

	sut.NewMapping(key)

	got,_ := sut.GetMapping(key)

	if got != want {
		t.Errorf("GetMapping() returned %v, want %v", got, want)
	}
}

func TestObjectMappingUnit_RemoveMapping(t *testing.T) {
	sut := NewObjectMappingUnit()

	key := "baz"
	want := "Key not mapped"

	sut.NewMapping(key)
	sut.RemoveMapping(key)

	_,got := sut.GetMapping(key) // should error

	if got.Error() != want {
		t.Errorf("GetMapping() returned error %v, want %v", got, want)
	}
}
