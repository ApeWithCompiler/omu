package omu

// ObjectMappingTable describes a abstract table for
// keeping reference of key value mappings	
type ObjectMappingTable interface {
	SetKey(key, value string) error
	UnsetKey(key string) error
	GetKey(key string) (string, error)
}
