package omu

import (
	"testing"
)

func TestGeneratorMock(t *testing.T) {
	sut := NewGeneratorMock()

	want := "foobar"
	got,_ := sut.GenerateValue(want)
	if got != want {
		t.Errorf("GenerateValue() returned %v, want %v", got, want)
	}
}
